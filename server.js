const express = require('express')
const app = express();
app.use(express.json());

const sql = require("msnodesqlv8");
//const { rows } = require("mssql");
const connectionString =" Driver={SQL Server Native Client 11.0};Server=10.150.1.94;Database=DBTAINING;Uid=sa;Pwd=KoreA#$856#$LaoS; "


app.listen(3000, () => console.log('server 3000 runing'));

// READ
app.get("/read", async (req, res) => {
    try {
        sql.query(connectionString,"SELECT * FROM tasks order by id", (err, results, fields) => {
            if (err) {
                console.log(err);
                return res.status(400).send();
            }
            res.status(200).json(results)
        })
    } catch(err) {
        console.log(err);
        return res.status(500).send();
    }
})




 /* const query ="select * from [Persons]";
sql.query(connectionString, query, (err,rows) =>{
    if (err) {
        console.log('error connect!', err)
        return;

    }
    console.log('connected',rows);
});  */



// CREATE Routes
app.post("/create", async (req, res) => {
    const { id, task, status } = req.body;

    try {
        sql.query(connectionString,
            "INSERT INTO tasks(id, task, status) VALUES(?, ?, ?)",
            [id, task, status],
            (err, results, fields) => {
                if (err) {
                    console.log("Error while inserting a user into the database", err);
                    return res.status(400).send();
                }
                return res.status(201).json({ message: "New user successfully created!"});
            }
        )
    } catch(err) {
        console.log(err);
        return res.status(500).send();
    }
})



// READ single users from db
app.get("/read/single/:task", async (req, res) => {
    const task = req.params.task;

    try {
        sql.query(connectionString,"SELECT * FROM tasks WHERE task like '%' + ? + '%' ", [task], (err, results, fields) => {
            if (err) {
                console.log(err);
                return res.status(400).send();
            }
            res.status(200).json(results)
        })
    } catch(err) {
        console.log(err);
        return res.status(500).send();
    }
})



// UPDATE data
app.patch("/update/:email", async (req, res) => {
    const email = req.params.email;
    const newPassword = req.body.newPassword;

    try {
        connection.query("UPDATE users SET password = ? WHERE email = ?", [newPassword, email], (err, results, fields) => {
            if (err) {
                console.log(err);
                return res.status(400).send();
            }
            res.status(200).json({ message: "User password updated successfully!"});
        })
    } catch(err) {
        console.log(err);
        return res.status(500).send();
    }
})



// DELETE
app.delete("/delete/:email", async (req, res) => {
    const email = req.params.email;

    try {
        connection.query("DELETE FROM users WHERE email = ?", [email], (err, results, fields) => {
            if (err) {
                console.log(err);
                return res.status(400).send();
            }
            if (results.affectedRows === 0) {
                return res.status(404).json({ message: "No user with that email!"});
            }
            return res.status(200).json({ message: "User deleted successfully!"});
        })
    } catch(err) {
        console.log(err);
        return res.status(500).send();
    }
})

